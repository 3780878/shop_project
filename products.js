var products = [
    {
        id: '1',
        name: 'Nokia 5',
        manufacturer: 'China',
        price: 15000,
        image: 'phone.jpeg',
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'android'
        }
    },
    {
        id: '2',
        name: 'Nokia 6',
        manufacturer: 'China',
        price: 15000,
        image: 'phone.jpeg',
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'android'
        }
    },
    {
        id: '3',
        name: 'Nokia 7',
        manufacturer: 'China',
        price: 15000,
        image: 'phone.jpeg',
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'android'
        }
    },
    {
        id: '4',
        name: 'Samsung EX 5565',
        manufacturer: 'China',
        price: 67000,
        image: 'komp.jpeg',
        attr:{
            camera:'6mp',
            diagonal: '16',
            os:'linux'
        }
    },
    {
        id: '5',
        name: 'Samsung XX 5576',
        manufacturer: 'China',
        price: 77000,
        image: 'komp.jpeg',
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'windows'
        }
    },
    {
        id: '6',
        name: 'Aser EX 5565',
        manufacturer: 'China',
        price: 16000,
        image: 'komp.jpeg',
        attr:{
            camera:'6mp',
            diagonal: '16',
            os:'linux'
        }
    },
    {
        id: '7',
        name: 'ASER XX 5576',
        manufacturer: 'China',
        price: 15000,
        image: 'tehnika.jpeg',
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'windows'
        }
    },
    {
        id: '8',
        name: 'Aser DD 4547',
        manufacturer: 'China',
        image: 'tehnika.jpeg',
        price: 15000,
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'linux'
        }
    },
    {
        id: '9',
        name: 'Aser LL 5544',
        manufacturer: 'China',
        image: 'tehnika.jpeg',
        price: 15000,
        attr:{
            camera:'5mp',
            diagonal: '5',
            os:'linux'
        }
    }

];